//
//  eWalletTests.swift
//  eWalletTests
//
//  Created by MALLOJJALA PAVAN TEJA on 3/20/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import XCTest
@testable import eWallet
class eWalletTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRatesViewModel() {
        
        let rates = BaseRates(rates: ["EUR":0.9258402,"MYR":4.4149615776,"USD":1.0,"SGD":1.4482918248], base: "USD", date: "2020-03-19")
        let ratesVm = ratesViewModel(rates: rates)
        XCTAssertEqual(rates.rates, ratesVm.rates)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
