//
//  eWalletUITests.swift
//  eWalletUITests
//
//  Created by MALLOJJALA PAVAN TEJA on 3/20/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import XCTest

class eWalletUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHomeScreen() {
        
        let app = XCUIApplication()
        let element = app.otherElements.containing(.navigationBar, identifier:"eWallet.View").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.children(matching: .textField).matching(identifier: "currency").element(boundBy: 0).tap()
        
        let sgdPickerWheel = app/*@START_MENU_TOKEN@*/.pickerWheels["SGD"]/*[[".pickers.pickerWheels[\"SGD\"]",".pickerWheels[\"SGD\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sgdPickerWheel/*@START_MENU_TOKEN@*/.press(forDuration: 1.6);/*[[".tap()",".press(forDuration: 1.6);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.tap()
        element.children(matching: .textField).matching(identifier: "currency").element(boundBy: 1).tap()
        sgdPickerWheel/*@START_MENU_TOKEN@*/.press(forDuration: 1.7);/*[[".tap()",".press(forDuration: 1.7);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.tap()
        app.textFields["from amount"].tap()
        
        let key = app/*@START_MENU_TOKEN@*/.keys["1"]/*[[".keyboards.keys[\"1\"]",".keys[\"1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        key.tap()
        key.tap()
        
        let key2 = app/*@START_MENU_TOKEN@*/.keys["0"]/*[[".keyboards.keys[\"0\"]",".keys[\"0\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        key2.tap()
        key2.tap()
        key2.tap()
        element.tap()
                
    }

}
