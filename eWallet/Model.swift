//
//  Model.swift
//  eWallet
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

struct BaseRates: Codable {
    let rates: Dictionary<String,Double>
    let base: String
    let date: String
}
