//
//  transactionHistoryCell.swift
//  eWallet
//
//  Created by MALLOJJALA PAVAN TEJA on 3/18/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class transactionHistoryCell: UICollectionViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var txnNumber: UILabel!
    @IBOutlet weak var txnDate: UILabel!
    @IBOutlet weak var txnAmount: UILabel!
    @IBOutlet weak var txnRate: UILabel!
    
    override func layoutSubviews() {
        baseView.layer.borderWidth = 2.0
        baseView.layer.borderColor = UIColor.black.cgColor

    }
}
