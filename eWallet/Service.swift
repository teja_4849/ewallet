//
//  Service.swift
//  eWallet
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

class Service: NSObject {
    static let shared = Service()
    
 
 
    
    func fetchImagesData(_ url: String, parameters: [String: String], completion: @escaping (BaseRates?, Error?) -> ()) {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        
     
        request.httpMethod = "GET"

        let decoder = JSONDecoder()
        
        URLSession.shared.dataTask(with: request) { data, response, err in
            if let err = err {
                completion(nil, err)
                print("error in fetching Images Data: \(err )")
                return
            }
            guard let Jsondata = data else { return }
            //print("yay! we got response :", String(decoding: Jsondata, as: UTF8.self))
            do {
                let ratesjson = try decoder.decode(BaseRates.self, from: Jsondata)
                DispatchQueue.main.async {
                    completion(ratesjson, nil)
                }
                
            }
            catch {
                print("the error is : \(error)")
            }
            }.resume()
        
    }
}
