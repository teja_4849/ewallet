//
//  transactionHistory.swift
//  eWallet
//
//  Created by MALLOJJALA PAVAN TEJA on 3/18/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class transactionHistory: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var txnHistoryData: [[String:Any]] = []

    @IBOutlet weak var transactionHistoryList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
let defaults = UserDefaults()
  
        if let cd = defaults.object(forKey: "convertData") as? [[String:Any]] {
            txnHistoryData = cd
            print("txnhistory count : ", cd.count)

        }
        // Do any additional setup after loading the view.
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return txnHistoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let txnItem = collectionView.dequeueReusableCell(withReuseIdentifier: "txnItem", for: indexPath) as! transactionHistoryCell
        var txnHistory = txnHistoryData[indexPath.row]
         let txndt = txnHistory["date"]
        let fromCurrency = txnHistory["fromCurrency"]
        let txnNumber = txnHistory["txnNumber"]
        let toCurrency = txnHistory["toCurrency"]
        let rate = txnHistory["rate"]
        let fromAmount = txnHistory["fromAmount"]
        let toAmount = txnHistory["toAmount"]
        let  txnAmount = "\(fromCurrency ?? "")\(fromAmount ?? "") -> \(toCurrency ?? "")\(toAmount ?? "")"
        
        txnItem.txnDate.text = txndt as! String
        txnItem.txnRate.text = rate as! String
        txnItem.txnAmount.text = txnAmount
        txnItem.txnNumber.text = txnNumber as! String
        return txnItem
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
