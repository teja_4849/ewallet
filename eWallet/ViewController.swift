//
//  ViewController.swift
//  eWallet
//
//  Created by MALLOJJALA PAVAN TEJA on 3/18/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
  

    @IBOutlet weak var fromCurrencyLabel: UILabel!
    @IBOutlet weak var fromCurrencyTf: UITextField!
    @IBOutlet weak var walletBalanceLabel: UILabel!
    @IBOutlet weak var walletBalanceDisplay: UILabel!
    @IBOutlet weak var fromAmountLabel: UILabel!
    @IBOutlet weak var fromAmountTf: UITextField!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var rateDisplay: UILabel!
    @IBOutlet weak var toCurrencyLabel: UILabel!
    @IBOutlet weak var toCurrencyTf: UITextField!
    @IBOutlet weak var toAmountLabel: UILabel!
    @IBOutlet weak var toamountTf: UITextField!
    @IBOutlet weak var convertBtn: UIButton!
    @IBOutlet weak var viewHistoryBtn: UIButton!
    
    var fromCurrencies = ["SGD","MYR","USD","EUR"]
    var toCurrencies = ["SGD","MYR","USD","EUR"]
    weak var pickerView: UIPickerView?
    var params:[String:String] = [:]
    var counter = 0
    var convertData: [String:Any] = [:]
    var convertDataArray: [[String:Any]] = []

    var fromRates:Dictionary<String,Double> = [:]
    var toRates:Dictionary<String,Double> = [:]

    var currencyName: String?
    var blnc = 1000.00 //TODO: need to userdefault

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        convertBtn.layer.borderWidth = 2.0
        convertBtn.layer.borderColor = UIColor.black.cgColor
        convertBtn.layer.cornerRadius = convertBtn.bounds.size.height / 2
        viewHistoryBtn.layer.borderWidth = 2.0
        viewHistoryBtn.layer.borderColor = UIColor.black.cgColor
        viewHistoryBtn.layer.cornerRadius = viewHistoryBtn.bounds.size.height / 2

        createPicker()
        dismissKeyboard()
    }
    
    func toggleToAmount()  {
        guard let fromamount = fromAmountTf.text else {return}
        let toCurrency = toCurrencyTf.text

        let mutiplier = self.fromRates.filter({$0.key==toCurrency}).map({$0.value}).first ?? 00
        print("from amounr \(fromamount) : \(toCurrency ?? "") and is multiplier \(String(describing: mutiplier))")
        if let doubleOffromamount = Double(fromamount) {
            let res = doubleOffromamount * mutiplier.round(to: 2)
            print(" \(doubleOffromamount * mutiplier) result : \(res)")
            toamountTf.text = "\(String(format: "%.2f", res))"

        }
    }

    func toggleFromAmount()  {
        guard let toAmount = toamountTf.text else {return}
        let fromCurrency = fromCurrencyTf.text
        let mutiplier = self.toRates.filter({$0.key==fromCurrency}).map({$0.value}).first ?? 00
        print("to amounr \(toAmount) : \(fromCurrency ?? "") and is multiplier \(String(describing: mutiplier))")
        if let doubleOfToamount = Double(toAmount) {
            let res = doubleOfToamount * mutiplier.round(to: 2)
            print(" \(doubleOfToamount * mutiplier) result : \(res)")
            fromAmountTf.text = "\(String(format: "%.2f", res))"

        }
    }

    
    //MARK: pickerview delegate methods
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let count = fromCurrencyTf.isFirstResponder ? fromCurrencies.count : toCurrencies.count
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let currenciesName = fromCurrencyTf.isFirstResponder ? fromCurrencies : toCurrencies

        return currenciesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if fromCurrencyTf.isFirstResponder {
                    let fromCurrencyName = fromCurrencies[row]
                fromCurrencyTf.text = fromCurrencyName
            getFromRates(base: fromCurrencyName)
        } else if toCurrencyTf.isFirstResponder{
            let toCurrencyName = fromCurrencies[row]
toCurrencyTf.text = toCurrencyName
            getToRates(base: toCurrencyName)
        }
       conversionRate()
    }
    
    
    
    func getFromRates(base: String) {
        let url = "https://api.exchangeratesapi.io/latest"
        if base == "EUR" {
            params["symbols"] = "SGD,MYR,USD"
            params["base"] = base
            Service.shared.fetchImagesData(url, parameters: params) { (rates, err) in
                self.fromRates = rates.map({ratesViewModel(rates: $0).rates}) ?? [:]
                self.walletBalanceDisplay.text = "\(self.blnc)"
            }
        } else {
         params["symbols"] = "SGD,MYR,USD,EUR"
         params["base"] = base
         Service.shared.fetchImagesData(url, parameters: params) { (rates, err) in
            self.fromRates = rates.map({ratesViewModel(rates: $0).rates}) ?? [:]
            print(self.blnc(base: base))
         }
        }
    }
    
    func getToRates(base: String) {
        let url = "https://api.exchangeratesapi.io/latest"
        if base == "EUR" {
            params["symbols"] = "SGD,MYR,USD"
            params["base"] = base
            Service.shared.fetchImagesData(url, parameters: params) { (rates, err) in
                self.toRates = rates.map({ratesViewModel(rates: $0).rates}) ?? [:]
            }
        } else {
            params["symbols"] = "SGD,MYR,USD,EUR"
            params["base"] = base
            Service.shared.fetchImagesData(url, parameters: params) { (rates, err) in
                self.toRates = rates.map({ratesViewModel(rates: $0).rates}) ?? [:]
            }
        }
    }
    
    func blnc (base: String) -> String {
        let url = "https://api.exchangeratesapi.io/latest"
        var returningBalance = ""
        var multiplier:Double = 0
        params["symbols"] = "SGD,MYR,USD"
        params["base"] = "EUR"
        Service.shared.fetchImagesData(url, parameters: params) { (rates, err) in
            let rts = rates.map({ratesViewModel(rates: $0).rates}) ?? [:]
            multiplier =  rts.filter({$0.key == base}).map({$0.value}).first ?? 0
            let balance = self.blnc * multiplier
            
            returningBalance = "\(balance.round(to: 2))"
            self.walletBalanceDisplay.text = returningBalance
    }
        return returningBalance
    }
    
    func conversionRate() {
        
        let from = fromCurrencyTf.text
        let to = toCurrencyTf.text
        
        let fromCurrency = self.fromRates.filter({$0.key == from}).map({$0.key}).first ?? ""
        let toCurrency = self.fromRates.filter({$0.key == to}).map({$0.key}).first ?? ""
        let fromRate = self.fromRates.filter({$0.key == from}).map({$0.value}).first ?? 0
        let toRate = self.fromRates.filter({$0.key == to}).map({$0.value}).first ?? 0

        let conversionRate = "\(getSymbol(of: fromCurrency))\(fromRate.round(to: 2)) to \(getSymbol(of: toCurrency))\(toRate.round(to: 2))"
        
        rateDisplay.text = conversionRate
        
    }
    
    func getSymbol(of cu: String) -> String {
        var symbol = ""
        
        if cu == "SGD" {
            symbol = "S$"
        } else if cu == "EUR" {
            symbol = "€"
        }else if cu == "MYR" {
            symbol = "RM"
        }else if cu == "USD" {
            symbol = "$"
        }
        return symbol
    }
    
    // MARK: textfeild methods
    func createPicker()  {
        let ratePicker = UIPickerView()
        ratePicker.delegate = self
        ratePicker.dataSource = self
         fromCurrencyTf.delegate = self
         toCurrencyTf.delegate = self
        fromCurrencyTf.inputView = ratePicker
        toCurrencyTf.inputView = ratePicker
        ratePicker.backgroundColor = UIColor.white
        self.pickerView = ratePicker
    }

    
    //MARK: handle Keyboard
    func dismissKeyboard()  {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gesture:)))
        self.view.addGestureRecognizer(tap)
        
    }
    @objc func handleTap (gesture: UITapGestureRecognizer) {
        if fromAmountTf.isFirstResponder {
toggleToAmount()
        }
        if toamountTf.isFirstResponder {
            toggleFromAmount()
        }
        view.endEditing(true)
     
    }

    func currentDate() -> String {
        let currentDateTime = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM YYYY - HH:mm"
        let dt = dateFormatter.string(from: currentDateTime)
        return dt
    }
    
    func showTxnHistory() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let txnHistory = storyBoard.instantiateViewController(withIdentifier: "txnHistory") as! transactionHistory
        self.navigationController?.pushViewController(txnHistory, animated: true)
    }
    
    @IBAction func convertAction(_ sender: Any) {
        let defaults = UserDefaults()
      //var currentCounter = defaults.object(forKey: "counter") as! Int
        var sendingCounter = 0
        if var currentCounter = defaults.object(forKey: "counter") as? Int {
            currentCounter += 1
            sendingCounter = currentCounter
            defaults.set(currentCounter, forKey: "counter")
        }  else {
            let initalCounter = 1
            sendingCounter = initalCounter
            defaults.set(initalCounter, forKey: "counter")
        }
        
        if let fromCurrency = fromCurrencyTf.text, !fromCurrency.isEmpty, let toCurrency = toCurrencyTf.text, !toCurrency.isEmpty, let rate = rateDisplay.text, !rate.isEmpty, let fromAmount = fromAmountTf.text, !fromAmount.isEmpty, let toAmount = toamountTf.text, !toAmount.isEmpty, let walletbal = walletBalanceDisplay.text, !walletbal.isEmpty {
            print("from amount\(fromAmount) : blnc \(walletbal)")
            guard  let sendingAmount = Double(fromAmount) else {return}
            guard let balance = Double(walletbal) else {return}
            
            if sendingAmount.isLess(than: balance){
            
            let alertMessage = UIAlertController(title: "Alert", message: "Are you sure you want to convert \(getSymbol(of: fromCurrency))\(fromAmount) to \(getSymbol(of: toCurrency))\(toAmount)", preferredStyle: .alert)
            
            alertMessage.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil
            ))
            
            alertMessage.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.convertData["fromCurrency"] = fromCurrency
                self.convertData["date"] = self.currentDate()
                self.convertData["txnNumber"] = "TX000\(sendingCounter)"
                self.convertData["toCurrency"] = toCurrency
                self.convertData["rate"] = rate
                self.convertData["fromAmount"] = fromAmount
                self.convertData["toAmount"] = toAmount
                
                if var cdA = defaults.object(forKey: "convertData") as? [[String:Any]] {
                    cdA.append(self.convertData)
                    defaults.set(cdA, forKey: "convertData")

                } else {
                    self.convertDataArray.append(self.convertData)
                    defaults.set(self.convertDataArray, forKey: "convertData")

                }

                print("sending data : ",self.convertDataArray)
                self.showTxnHistory()
            }))
            
            self.present(alertMessage, animated: true, completion: nil)
            } else {
                let alertMessage = UIAlertController(title: "Insufficient funds", message: "please verify the wallet balance and proceed with conversion", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil
                ))
                self.present(alertMessage, animated: true, completion: nil)
            }
        } else {
            let alertMessage = UIAlertController(title: "missing feilds", message: "please fill all feilds to proceed with conversion", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil
            ))
            self.present(alertMessage, animated: true, completion: nil)

        }
    }
    
    @IBAction func viewHistoryAction(_ sender: Any) {
        
       showTxnHistory()
    }
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
